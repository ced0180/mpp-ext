# mpp-fun-server
A version of [mpp-server](https://github.com/BopItFreak/mpp-server) with more features

## todo
feature list in readme  
option that lets you save custom users  
fix quota  

## config options
`port`: Which port the WebSocket server will listen on.  
`motd`: Can be virtually anything. It's unused on the normal client, but may break NMPBs if it's not a string.  
`_id_PrivateKey`: Used to help randomize generated _ids, prevents reversing the hashing algorithm.  
`defaultUsername`: Default username people will be given if they haven't connected before.  
`defaultRoomColor`: Default background color for normal rooms.  
`defaultLobbyColor`: Inner color for the lobby's background.  
`defaultLobbyColor2`: Outer color for the lobby's background.  
`adminpass`: Password used for the `admin message` message.  
`customUsersUseExisting`: Whether to allow custom users to use _ids of users that already exist  
